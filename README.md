# Custom-Elements creator

- it creates all the boilerplate for custom-elements
- it provides an example of how it works
- check out the console

**How it works**

1. run `npm run setup`
2. run `create-component`
3. checkout the boilerplate and example custom-element
4. modify it and make it your own
5. when you're happy, run `npm run build` in the custom-element directory
6. you'll find your "ready-to-go" element in the `_ready` directory
7. be happy and stay a good person
8. That's it
