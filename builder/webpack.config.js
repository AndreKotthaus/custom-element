const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const cleanPlugin = new CleanWebpackPlugin();
const terserPlugin = new TerserPlugin();

const name = process.argv[5];
const input = path.resolve(process.cwd(), '..', 'src', name, 'component', 'index.mjs');
const output = path.resolve(__dirname, '..', '_ready', name);

module.exports = {
    mode : 'production',
    entry: {
        main: input,
    },
    output: {
        filename: name + '.js',
        path    : output,
    },
    optimization: {
        minimizer: [terserPlugin],
    },
    plugins: [cleanPlugin],
};
