const inquirer = require('inquirer');

const isValidInput = input => input !== '' && /^[a-z0-9]+$/i.test(input);

const questions = {
    webcomponent: [
        {
            type    : 'input',
            name    : 'name',
            message : 'What is the name of the Component [a-z0-9]? Usage: (<namespace-NAME>)',
            validate: function (value) {
                return isValidInput(value) ? true : 'You have to enter a valid name of the Component [a-z0-9]';
            },
            filter: function (val) {
                return val.toLowerCase();
            },
        },
        {
            type    : 'input',
            name    : 'namespace',
            default : 'wgd',
            message : 'What is the namespace of the Component [a-z0-9]? Usage: (<NAMESPACE-name>)',
            validate: function (value) {
                return isValidInput(value) ? true : 'You have to enter a valid namespace of the Component [a-z0-9]';
            },
            filter: function (val) {
                return val.toLowerCase();
            },
        },
        {
            type   : 'confirm',
            name   : 'launch',
            default: true,
            message: 'Launch VScode automatically?',
        },
    ],
};

const ask = question => inquirer.prompt(question);

module.exports = { questions, ask };
