const fsp = require('fs').promises;
const Listr = require('listr');
const execa = require('execa');
const path = require('path');

/* Templates for webcompnents  */
const { createDefaultValues } = require('./../../templates/webcomponent/defaultvalues');
const { createCustomError } = require('./../../templates/webcomponent/error');
const { createHTML } = require('./../../templates/webcomponent/html');
const { createIndex } = require('./../../templates/webcomponent/index');
const { createPackage } = require('./../../templates/webcomponent/package');
const { createPreview } = require('./../../templates/webcomponent/preview');
const { createInfo } = require('./../../templates/webcomponent/info');

const basetarget = path.resolve(__dirname, '../../..', 'src');

const create = async (targetdir, name, namecap, namespace, namespacecap, launchvsc) => {
    const tasks = new Listr([
        {
            title: 'creating component Folder',
            task : async () => {
                await fsp.mkdir(targetdir);
                return await fsp.mkdir(path.normalize(`${targetdir}/component`));
            },
        },
        {
            title: 'create defaultvalues.mjs',
            task : async () => await fsp.writeFile(`${targetdir}/component/defaultvalues.mjs`, createDefaultValues(namespace), 'utf8'),
        },
        {
            title: 'create error.mjs',
            task : async () => await fsp.writeFile(`${targetdir}/component/error.mjs`, createCustomError(namespacecap), 'utf8'),
        },
        {
            title: 'create html.mjs',
            task : async () => await fsp.writeFile(`${targetdir}/component/html.mjs`, createHTML(), 'utf8'),
        },
        {
            title: 'create index.mjs',
            task : async () => await fsp.writeFile(`${targetdir}/component/index.mjs`, createIndex(namespace, namespacecap, namecap, name), 'utf8'),
        },
        {
            title: 'create info.mjs',
            task : async () => await fsp.writeFile(`${targetdir}/component/info.mjs`, createInfo(namespace), 'utf8'),
        },
        {
            title: 'create index.html',
            task : async () => await fsp.writeFile(`${targetdir}/index.html`, createPreview(namespace, name), 'utf8'),
        },
        {
            title: 'create package.json',
            task : async () => await fsp.writeFile(`${targetdir}/package.json`, createPackage(namespace, name), 'utf8'),
        },
        {
            title  : 'Open VScode',
            task   : async () => await execa('code', [`${basetarget}/${namespace}-${name}`]),
            enabled: () => launchvsc,
        },
    ]);
    return await tasks.run();
};

const webcomponent = async ({ name, namespace, launch }) => {
    const capitalized = str => str[0].toUpperCase() + str.slice(1);
    const namecap = capitalized(name);
    const namespacecap = capitalized(namespace);
    const launchvsc = launch;
    const targetdir = path.normalize(path.join(basetarget, `${namespace}-${name}`));
    const isAllowed = await fsp.access(targetdir).catch(() => true);
    if (!isAllowed) throw new Error('This webcomponent already exists!');

    return await create(targetdir, name, namecap, namespace, namespacecap, launchvsc);
};

module.exports = webcomponent;
