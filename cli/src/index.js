const chalk = require('chalk');
const { questions, ask } = require('./questions');
const webcomponent = require('./create/webcomponent');

async function cli() {
    const details = await ask(questions['webcomponent']);
    const comp = await webcomponent(details).catch(e => ({ error: e.message }));
    if (comp.error) return console.error(chalk.red.bold(comp.error));
    console.log(`%s your webcomponent is available as <${details.namespace}-${details.name}> `, chalk.green.bold('Done'));
}

exports.cli = cli;
