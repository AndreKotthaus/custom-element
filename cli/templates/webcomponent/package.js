const createPackage = (namespace, name) => `
{
	"name": "${namespace}-${name}",
	"version": "1.0.0",
	"description": "",
	"main": "index.js",
	"scripts": {
		"build": "npm run build ${namespace}-${name} --prefix ./../../builder "
	},
	"keywords": [],
	"author": "",
	"license": "ISC"
}`;

module.exports = {
    createPackage,
};
