const createHTML = () => `
export const html = () => \`
<style>
* {
	box-sizing: border-box;
}

.btn {
	--color: var(--color-on-primary, white);
	--background: var(--color-primary, black);
	color: var(--color);
	background-color: var(--background);
	cursor: pointer;
	border-radius: 2px;
	letter-spacing: 0.5px;
	width:inherit;
  }
  
  .btn:focus,
  btn:active {
	border: 1px solid var(--color-secondary);
  }
  
  .btnicon, .btn>span {
	pointer-events: none;  
}

.btnicon{
width:24px;
height:24px;
}

.big{
	height: 36px;
	line-height: 36px;
	text-decoration: none;
	border: none;
	outline: none;
	text-align: center;
	display: flex;
	align-items: center;
	justify-content: center;
	gap: 10px;
	padding: 0 16px;
	text-transform: uppercase;
	box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 1px 5px 0 rgba(0, 0, 0, 0.1);
}

.small{
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  font-size: 0.8rem;
  cursor: pointer;
  position: relative;
  align-items: center;
  outline: none;
  border: 1px solid transparent;
  padding:5px 10px;
}

</style>
<button class="btn small">
<slot name="icon" class="btnicon"></slot>	
<span id="text"></span>
</button>
\`;
`;

module.exports = {
    createHTML,
};
