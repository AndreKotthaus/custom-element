const createCustomError = namespacecap => `
export class ${namespacecap}Error extends Error {
	constructor(message) {
	  super(message);
	  this.name = '${namespacecap}Error';
	  this.message = message;
	}
	toJSON() {
	  return {
		error: {
		  name: this.name,
		  message: this.message,
		  stacktrace: this.stack,
		},
	  };
	}
  }
`;

module.exports = {
    createCustomError,
};
