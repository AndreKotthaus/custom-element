const createInfo = namespace => `
export const info = {
	props: {
	  info: {
		description: 'This very prop',
		type: 'String',
		example: 'No value',
		default: '',
	  },
	  text: {
		description: 'The text for this button',
		type: 'String',
		example: 'Hello World',
		default: 'Click me!',
	  },
	  size: {
		description: 'The size of this button',
		type: 'String',
		example: 'small',
		default: 'big',
	  },
	},
	events: {
	  '${namespace}-error': {
		description: 'Emits the internal error event to the outside',
		type: 'Event',
		example: 'el.addEventListener("${namespace}-error", handleFN)',
	  },
	  '${namespace}-click': {
		description: 'Emits the test of the button',
		type: 'Event',
		example: 'el.addEventListener("${namespace}-click", handleFN)',
	  },
	},
  };
`;

module.exports = {
    createInfo,
};
