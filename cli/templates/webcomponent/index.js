const createIndex = (namespace, namespacecap, namecap, name) => `
import { html } from './html.mjs';
import { defaultvalues } from './defaultvalues.mjs';
import { ${namespacecap}Error } from './error.mjs';
import { info } from './info.mjs';


/******************************************
 *
 * GENERAL INFO:
 *
 * The eventstack of a webcomponent fires
 * in the following order:
 *
 * -> constructor
 * -> attributeChangedCallback
 * -> connectedCallback
 * --> availabe for the outside world
 *
 * IN THIS PARTICULAR VERSION:
 *
 * All Methods starting with __
 * are automatically observed Attributes
 *
 ******************************************/

class ${namecap} extends HTMLElement {
	
  /******************************************
   * Necessary Methods for all custom-elements
   ******************************************/
  
	constructor() {
	  super();
	  for (let key in defaultvalues) this[key] = defaultvalues[key];
	  this.shadow = this.attachShadow({ mode: 'closed' });
	}

	connectedCallback() {
	  this.shadow.innerHTML = html();
	  this.getElements();
	  this.setEvents();
	  this.setSize();
	  this.setText();
	}
	
	disconnectedCallback() {
	  this.removeEvents();
	}
	
	/******************************************
	 * Methods to observe HTML attributes
	 ******************************************/
	
	static get observedAttributes() {
		return Object.getOwnPropertyNames(this.prototype)
		.filter(item => item.indexOf('__') === 0)
	  	.map(item => item.substring(2));
	}
	
	attributeChangedCallback(attr, oldValue, newValue) {
		if (newValue !== '' && newValue !== oldValue) return this[\`__\${attr}\`](newValue);
	}
	
	/******************************************
	 * Helper Methods for this component
	 ******************************************/
	
	getElements() {
		this.button = this.shadow.querySelector('.btn');
		this.buttontext = this.button.querySelector('#text');
	}
	
	setEvents() {
		this.button.addEventListener('click', this.buttonClick.bind(this));
	}
	
	removeEvents() {
		this.button.removeEventListener('click', this.buttonClick.bind(this));
	}
	
	emit({ eventName, eventDetails } = {}) {
		
		/* Create the custom event */
		const e = new CustomEvent(eventName, {
		  bubbles: true,
		  cancelable: false,
		  composed: true,
		  detail: eventDetails,
		});
		
		/* Move execution to the end of the event stack */
    	setTimeout(O => this.shadow.dispatchEvent(e), 0);
	}
	
	/******************************************
	 * Methods for this specific component
	 ******************************************/
	
	buttonClick(e) {
		e.target.blur();
		return this.emit({
			eventName: \`${namespace}-click\`,
			eventDetails: this.textvalue,
		  });
	}

	/* Set size of the button */
	setSize() {
	  if (!this.size || !this.button || this.button.className.indexOf(this.size) > -1) return;
	  this.button.className = \`btn \${this.size}\`;
	}

	/* Set text of the button */
	setText() {
	  if (!this.textvalue || !this.buttontext) return;
	  this.buttontext.innerText = this.textvalue;
	}

	/******************************************
	 * Methods for HTML attributes
	 ******************************************/
	
	__text(value) {
	  this.textvalue = value;
	  this.setText();
	}

	__size(value) {
	  if (this.allowedsize && this.allowedsize.indexOf(value) === -1) {
		return this.emit({
		  eventName: \`${namespace}-error\`,
		  eventDetails: new ${namespacecap}Error('Wrong size for this button'),
		});
	  }
	  this.size = value;
	  this.setSize();
	}

	__info() {
		console.log('%c Props:', 'font-weight:bold;');
		console.table(info.props);
		console.log('%c Events:', 'font-weight:bold;');
		console.table(info.events);
	}
}

customElements.define(\`${namespace}-${name}\`, ${namecap});

`;

module.exports = {
    createIndex,
};
