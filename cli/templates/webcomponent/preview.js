const createPreview = (namespace, name) => `
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>component ${namespace}-${name}</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script src="./component/index.mjs" type="module"></script>
</head>

<body>
	<${namespace}-${name} info="true">
		<i slot="icon" class="material-icons" id="icontype">check_circle</i>
	</${namespace}-${name}>
</body>
<script>
	const el = document.querySelector('${namespace}-${name}');
	el.addEventListener('${namespace}-click', e => {
		alert('Check the Console (maybe needs a reload for table-display) ');
	})
	el.addEventListener('${namespace}-error', e => {
		console.log('Error: ', e.detail)
	})

	window.addEventListener('error', e => {
		console.log(e.error)

	})
</script>

</html>
`;

module.exports = { createPreview };
