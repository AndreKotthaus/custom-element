
import { html } from './html.mjs';
import { defaultvalues } from './defaultvalues.mjs';
import { WgdError } from './error.mjs';

/******************************************
 * All Methods starting with __
 * are automatically observed Attributes
 ******************************************/

class Test extends HTMLElement {
	
  /******************************************
   * Necessary Methods for all custom-elements
   ******************************************/
  
	constructor() {
	  super();
	  for (let key in defaultvalues) this[key] = defaultvalues[key];
	  this.shadow = this.attachShadow({ mode: 'closed' });
	}

	connectedCallback() {
	  this.shadow.innerHTML = html();
	  this.getElements();
	  this.setEvents();
	  this.setSize();
	  this.setText();
	}
	
	disconnectedCallback() {
	  this.removeEvents();
	}
	
	/******************************************
	 * Methods to observe HTML attributes
	 ******************************************/
	
	static get observedAttributes() {
		return Object.getOwnPropertyNames(this.prototype)
		.filter(item => item.indexOf('__') === 0)
	  	.map(item => item.substring(2));
	}
	
	attributeChangedCallback(attr, oldValue, newValue) {
		if (newValue !== '' && newValue !== oldValue) return this[`__${attr}`](newValue);
	}
	
	/******************************************
	 * Helper Methods for this component
	 ******************************************/
	
	getElements() {
		this.button = this.shadow.querySelector('.btn');
		this.buttontext = this.button.querySelector('#text');
	}
	
	setEvents() {
		this.button.addEventListener('click', this.buttonClick.bind(this));
	}
	
	removeEvents() {
		this.button.removeEventListener('click', this.buttonClick.bind(this));
	}
	
	emit({ eventName, eventDetails } = {}) {
		const e = new CustomEvent(eventName, {
		  bubbles: true,
		  cancelable: false,
		  composed: true,
		  detail: eventDetails,
		});
		/* Move to end of the event stack */
		setTimeout(() => {
		  this.shadow.dispatchEvent(e);
		}, 0);
	}
	
	/******************************************
	 * Methods for this specific component
	 ******************************************/
	
	buttonClick(e) {
		e.target.blur();
		alert(this.textvalue);
	}

	/* Set size of the button */
	setSize() {
	  if (!this.size || !this.button || this.button.className.indexOf(this.size) > -1) return;
	  this.button.className = `btn ${this.size}`;
	}

	/* Set text of the button */
	setText() {
	  if (!this.textvalue || !this.buttontext) return;
	  this.buttontext.innerText = this.textvalue;
	}

	/******************************************
	 * Methods for HTML attributes
	 ******************************************/
	
	__text(value) {
	  this.textvalue = value;
	  this.setText();
	}

	__size(value) {
	  if (this.allowedsize && this.allowedsize.indexOf(value) === -1) {
		return this.emit({
		  eventName: `wgd-error`,
		  eventDetails: new WgdError('Wrong size for this button'),
		});
	  }
	  this.size = value;
	  this.setSize();
	}
}

customElements.define(`wgd-test`, Test);

