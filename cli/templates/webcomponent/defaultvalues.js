const createDefaultValues = namespace => `
export const defaultvalues = {
	/* Component namespace */
	namespace: '${namespace}',

	/* attribute size */
	allowedsize: ['big', 'small'],
	size: 'big',

	/* attribute text */
	textvalue: 'Click me!',

	/* Element Button */
	button: null,

	/* Element ButtonText */
	buttontext: null,
};
`;

module.exports = {
    createDefaultValues,
};
